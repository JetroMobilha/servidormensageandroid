package servico.jetro.servidordemensagem;

/**
 * Created by Jetro Domigos on 20/12/2017.
 *
 */

public class Constantes {
    public static String REQUISICAO = "requisicao";
    public static String ESTADO = "estado";
    public static String MENSAGEM = "mensagens";
    public static String OBJETO = "objeto";
    public static String CONTACTO_1 = "contacto_1";
    public static String CODIGO = "codigo";
}

package servico.jetro.servidordemensagem.Servico;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.List;

import servico.jetro.servidordemensagem.App;
import servico.jetro.servidordemensagem.BaixarDados;
import servico.jetro.servidordemensagem.Banco_de_dados.Crud;
import servico.jetro.servidordemensagem.Notificacao;
import servico.jetro.servidordemensagem.ObjetoServico;


public class ServicoVerificaMensagen extends IntentService {


    public ServicoVerificaMensagen() {
        super("verificaMensagens");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            if (App.getInstance().coneccao()){
                //todo:  verificar se tem produtos atualizados
                List<ObjetoServico> servico = BaixarDados.getInstance().servicos();
                if (servico != null && servico.size() > 0) {
                    for (ObjetoServico o : servico) {
                        Crud.getInstance().criaMensagens(o);
                        App.vindasDoServidor = App.vindasDoServidor + 1;
                    }
                }
            }

            Notificacao.notify(getApplicationContext());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

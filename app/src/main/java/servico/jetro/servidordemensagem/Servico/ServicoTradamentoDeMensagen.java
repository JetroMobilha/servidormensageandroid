package servico.jetro.servidordemensagem.Servico;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.List;

import servico.jetro.servidordemensagem.App;
import servico.jetro.servidordemensagem.BaixarDados;
import servico.jetro.servidordemensagem.Banco_de_dados.Crud;
import servico.jetro.servidordemensagem.Constantes;
import servico.jetro.servidordemensagem.Notificacao;
import servico.jetro.servidordemensagem.ObjetoServico;


public class ServicoTradamentoDeMensagen extends IntentService {


    public ServicoTradamentoDeMensagen() {
        super("verificaMensagens");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            if (intent.getExtras() != null && intent.getExtras().getSerializable(Constantes.OBJETO)!=null) {
                ObjetoServico objetoServico = (ObjetoServico) intent.getExtras().getSerializable(Constantes.OBJETO);
                App.getObjetoServicos().remove(objetoServico);
                Crud.getInstance().apagarUsuario(objetoServico.getCodigo());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

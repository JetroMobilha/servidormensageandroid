package servico.jetro.servidordemensagem.Servico;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import servico.jetro.servidordemensagem.App;
import servico.jetro.servidordemensagem.Notificacao;
import servico.jetro.servidordemensagem.ObjetoServico;
import servico.jetro.servidordemensagem.Servico.ServicoMensagen;

public class MsmEntregue extends BroadcastReceiver {

    public MsmEntregue() {
    }

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {


        Log.d("MsmEntregue", " ----------------SMS entregue----------------------");

        try {
            if (intent.getExtras().getSerializable("numero") != null){
                ObjetoServico servico = (ObjetoServico)   intent.getExtras().getSerializable("numero");
                Log.d("MsmEntregue", " SMS entregue " +servico.getNumero());
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

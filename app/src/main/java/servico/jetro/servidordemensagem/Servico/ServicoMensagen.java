package servico.jetro.servidordemensagem.Servico;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import servico.jetro.servidordemensagem.Notificacao;

public class ServicoMensagen extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final Handler handler = new Handler();
        Log.d("Servidor Mensagem","------------------- Run --------------------------");
        startService(new Intent(getApplicationContext(), ServicoVerificaMensagen.class));
        startService(new Intent(getApplicationContext(), ServicoEnviarMensagen.class));

        handler.postDelayed(new Runnable() {
            @Override
            public void run() { startService(new Intent(getApplicationContext(),ServicoMensagen.class));
            }
        },5000);

        Notificacao.notify(getApplicationContext());

        return START_STICKY;
    }
}

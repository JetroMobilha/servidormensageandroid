package servico.jetro.servidordemensagem.Servico;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReceiverApp extends BroadcastReceiver {

    public ReceiverApp() {
    }

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        // usadoa para Atualizar os dados da aplicação
        Intent inte2 = new Intent(context, ServicoMensagen.class);
        PendingIntent pendingIntent2 = PendingIntent.getService(context, 0, inte2, 0);
          assert alarmManager != null;
        alarmManager.set(AlarmManager.RTC, 5000, pendingIntent2);
    }
}

package servico.jetro.servidordemensagem.Servico;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import servico.jetro.servidordemensagem.App;
import servico.jetro.servidordemensagem.Banco_de_dados.Crud;
import servico.jetro.servidordemensagem.ObjetoServico;
import servico.jetro.servidordemensagem.Sms;

public class ServicoEnviarMensagen extends IntentService {

    public ServicoEnviarMensagen() {
        super("EnviaMensagens");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            int posicao = 0;
            while (true) {
                List<ObjetoServico> servico = new ArrayList<>();
                List<ObjetoServico> db = Crud.getInstance().getMensagens(posicao);

                if (db != null && db.size() > 0) {

                    for (ObjetoServico servico1 : db) {
                        if (!App.getObjetoServicos().contains(servico1) && !App.getObjetoFalhados().contains(servico1)) {
                            servico.add(0,servico1);
                        }
                    }
                    posicao = db.get(db.size() - 1).getId();
                }

                if (App.getObjetoFalhados().size() > 0) {

                    // recuperando objetos falhados
                    for (ObjetoServico servico1 : App.getObjetoFalhados()) {
                        if (!servico.contains(servico1)) {
                            servico.add(0,servico1);
                        }
                    }

                    // removendo objetos falhados
                    for (ObjetoServico servico1 : servico) {
                        if (App.getObjetoFalhados().contains(servico1)) {
                            App.getObjetoFalhados().remove(servico1);
                        }
                    }
                }

                // enviando as mensagems
                if ( servico.size() > 0) {
                    for (ObjetoServico o : servico) {
                        Sms.enviaSMS(getApplicationContext(), o);
                        App.addObjetoServicos(o);
                    }
                } else {
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

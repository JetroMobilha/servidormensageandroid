package servico.jetro.servidordemensagem.Servico;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

import servico.jetro.servidordemensagem.App;
import servico.jetro.servidordemensagem.Constantes;
import servico.jetro.servidordemensagem.Notificacao;
import servico.jetro.servidordemensagem.ObjetoServico;
import servico.jetro.servidordemensagem.Servico.ServicoTradamentoDeMensagen;

public class MsmSusseco extends BroadcastReceiver {

    ObjetoServico objetoServico;

    public MsmSusseco() {
    }

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {


        if (intent.getExtras().getSerializable("numero") != null){
          objetoServico = (ObjetoServico)  intent.getExtras().getSerializable("numero");
        }
        Log.d("MsmSusseco", " ----------------SMS MsmSusseco----------------------");

        try {

            switch (getResultCode()) {
                case Activity.RESULT_OK:

                    if (objetoServico != null){
                        Log.d("MsmSusseco","numero :" + objetoServico.getNumero());
                        App.enviadras = App.enviadras +1;
                        Notificacao.notify(context);
                        context.startService(new Intent(context, ServicoTradamentoDeMensagen.class).putExtra(Constantes.OBJETO,objetoServico));
                    }
                    Log.d("MsmSusseco", " ok " + intent.getIntExtra("object", 0));
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                      addFalha(objetoServico);
                    Log.d("MsmSusseco", " SMS generic failure "+ intent.getIntExtra("object", 0));
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    addFalha(objetoServico);
                    Log.d("MsmSusseco", " SMS no service"+ intent.getIntExtra("object", 0));
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    addFalha(objetoServico);
                    Log.d("MsmSusseco", " SMS null PDU "+ intent.getIntExtra("object", 0));
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    addFalha(objetoServico);
                    Log.d("MsmSusseco", " SMS radio off "+ intent.getIntExtra("object", 0));
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void addFalha(ObjetoServico objetoServico){
        App.getObjetoServicos().remove(objetoServico);
        App.addObjetoFalhados(objetoServico);
    }
}





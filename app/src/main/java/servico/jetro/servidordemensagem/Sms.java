package servico.jetro.servidordemensagem;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.ArrayList;

import servico.jetro.servidordemensagem.Servico.MsmEntregue;
import servico.jetro.servidordemensagem.Servico.MsmSusseco;

public class Sms {

    /**
     * Método responsável por enviar mensagens SMS.
     * @para mnumeroTelefone numero de telefone
     * @para mmensagem sms
     */

    public static void enviaSMS(String numeroTelefone, String mensagem) {
        SmsManager sms = SmsManager.getDefault();

        Log.d("SMS", numeroTelefone + " ----  " + mensagem );
// Divide a mensagem em partes
        ArrayList<String> mensagemArray = sms.divideMessage(mensagem);

// Envia cada parte da mensagem em um SMS
        sms.sendMultipartTextMessage(numeroTelefone, null, mensagemArray,null ,null);
    }

    public static void enviaSMS(Context context, ObjetoServico objetoServico) {
        SmsManager sms = SmsManager.getDefault();
        ArrayList<PendingIntent> entregues = new ArrayList<>();
        ArrayList<PendingIntent> msmSussecos = new ArrayList<>();

// Divide a mensagem em partes
        ArrayList<String> mensagemArray = sms.divideMessage(objetoServico.getMensagem());

// Envia cada parte da mensagem em um SMS
/*
        for (String ignored : mensagemArray){
            entregues.add(null);
            msmSussecos.add(null);
        }
*/
        entregues.add(PendingIntent.getBroadcast(context
                ,1992
                ,new Intent("servico.jetro.servidor.mensagem.MsmEntregue",null,context,MsmEntregue.class).putExtra("numero",objetoServico)
                ,0));

        msmSussecos.add(PendingIntent.getBroadcast(context
                ,1998
                ,new Intent("servico.jetro.servidor.mensagem.MsmSusseco",null,context,MsmSusseco.class).putExtra("numero",objetoServico)
                ,0));


        sms.sendMultipartTextMessage(objetoServico.getNumero(), null, mensagemArray,msmSussecos ,entregues);
    }
}

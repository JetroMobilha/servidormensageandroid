package servico.jetro.servidordemensagem.Banco_de_dados;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import servico.jetro.servidordemensagem.ObjetoServico;


@SuppressWarnings("NonAsciiCharacters")
public class Crud extends BDHelper {

    private static Crud crud;
    private static SQLiteDatabase database;
    private static int CONT = 0;

    private Crud(Context context) {
        super(context);
    }

    // TODO : Usuario
    public synchronized boolean criaMensagens(ObjetoServico usuario) {

        long confirma = 0;

        if (isUsuarioExiste(usuario.getCodigo())) {
            return actualizarUsuario(usuario);
        } else {
            SQLiteDatabase database = escriverBD();

            try {
                confirma = database.insert(TB, null, getContentValuesEntidades(usuario));
            } catch (Exception e) {
                //  e.printStackTrace();
            }
        }

        fecharConecao();
        return getBoolean(confirma);
    }

    public ObjetoServico getMensagens(String codigo) {

        String[] mId = new String[]{codigo};
        ObjetoServico usuario = null;
        SQLiteDatabase database = lerBD();

        //
        Cursor cursor = null;
        try {
            cursor = database.query(
                   TB, null,
                     CODIGO + "=?",
                    mId,
                    null,
                    null,
                    null);

            usuario = criarEntidadesCursor(cursor);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (cursor != null) cursor.close();
        }

        fecharConecao();
        return usuario;
    }

    private ContentValues getContentValuesEntidades(ObjetoServico usuario) {

        ContentValues values = new ContentValues();
        values.put(CODIGO, usuario.getCodigo());
        values.put(MENSAGEM, usuario.getMensagem());
        values.put(NUMERO, usuario.getNumero());
        return values;
    }

    private boolean isUsuarioExiste(String codigo) {

        String[] mId = new String[]{codigo};
        boolean usuario = false;
        SQLiteDatabase database = lerBD();

        //
        Cursor cursor = null;
        try {
            cursor = database.query(
                   TB, null,
                    CODIGO + "=?",
                    mId,
                    null,
                    null,
                    null);

            if (cursor.getCount() > 0) {

                usuario = true;

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (cursor != null) cursor.close();
        }

        fecharConecao();
        return usuario;
    }

    public boolean actualizarUsuario(ObjetoServico usuario) {

        String[] mId = new String[]{usuario.getCodigo()};
        int confirma = 0;

        SQLiteDatabase database = escriverBD();

        try {
            confirma = database.update(TB, getContentValuesEntidades(usuario),CODIGO + "=?", mId);

        } catch (Exception e) {
            e.printStackTrace();
        }
        fecharConecao();

        return getBoolean(confirma);
    }

    public void apagarUsuario(String codigo) {

        long conf = 0;

        SQLiteDatabase database = escriverBD();

        try {
            conf = database.delete(TB,
                    CODIGO + "= ? "
                    , new String[]{codigo});
        } catch (Exception e) {
            e.printStackTrace();
        }

        fecharConecao();
    }

    public synchronized List<ObjetoServico> getMensagens(int posicao) {

        String query;
        String query1 =   _ID + ">=? AND " +  _ID + "<=?";
        String query2 =  _ID + ">? AND " +  _ID + "<=?";

        if (posicao == 0) {
            query = query1;
            posicao = minimo(TB, _ID);
        } else {
            query = query2;
        }

        int tamanho = 20;
        int id = posicao;
        int fim = posicao + tamanho;
        int items = 0;

        int maximoId = maximo(TB ,_ID);

        SQLiteDatabase database = lerBD();
        List<ObjetoServico> lista = new ArrayList<>();

        Cursor cursor = null;

        if (id == 0 || maximoId == 0) items = tamanho;
        try {

            while (items < tamanho) {

                String[] mcategoria = new String[]{String.valueOf(id), String.valueOf(fim)};
                cursor = database.query(TB, null, query, mcategoria, null, null, null);

                criarProdutosCursor(cursor, lista, items, tamanho);

                if (fim > maximoId)
                    break;

                id = fim;
                fim += tamanho;
                query = query2;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        fecharConecao();
        return lista;
    }

    public static synchronized Crud getInstance() {

        if (crud == null) {
            throw new IllegalStateException(Crud.class.getSimpleName() +
                    " is not initialized, call init(..) method first.");
        }
        return crud;
    }

    private static synchronized SQLiteDatabase lerBD() {
        CONT++;
        database = crud.getReadableDatabase();
        return database;
    }

    private static synchronized SQLiteDatabase escriverBD() {
        CONT++;
        database = crud.getWritableDatabase();
        return database;
    }

    private static synchronized void fecharConecao() {

        CONT--;
        if (CONT == 0 || CONT < 0)
            database.close();
    }

    public static synchronized void init(Context context) {
        if (crud == null) {
            crud = new Crud(context);
        }
    }


    private void criarProdutosCursor(Cursor cursor, List<ObjetoServico> list, int con, int quantidade) {

        try {
            while (cursor.moveToNext()) {
                con++;
                ObjetoServico objeto = new ObjetoServico();
                objeto.setId(cursor.getInt(cursor.getColumnIndex(_ID)));
                objeto.setCodigo(cursor.getString(cursor.getColumnIndex(CODIGO)));
                objeto.setNumero(cursor.getString(cursor.getColumnIndex(NUMERO)));
                objeto.setMensagem(cursor.getString(cursor.getColumnIndex(MENSAGEM)));

                list.add(objeto);

                if (con >= quantidade)
                    break;
            }
        } finally {
            cursor.close();
        }
    }




    private ObjetoServico criarEntidadesCursor(Cursor cursor) {
        ObjetoServico objeto = null;
        try {
            while (cursor.moveToNext()) {
                objeto = new ObjetoServico();


                objeto.setId(cursor.getInt(cursor.getColumnIndex(_ID)));
                objeto.setCodigo(cursor.getString(cursor.getColumnIndex(CODIGO)));
                objeto.setMensagem(cursor.getString(cursor.getColumnIndex(MENSAGEM)));
                objeto.setNumero(cursor.getString(cursor.getColumnIndex(NUMERO)));
            }
        } finally {
            cursor.close();
        }
        return objeto;
    }

    public int contador(String nomeTabela) {

        int retorno = 0;

        try {

            String STRING = "SELECT COUNT(*) FROM " + nomeTabela;
            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public int maximo(String nomeTabela, String nomeCulona) {


        int retorno = -1;

        try {

            String STRING = "SELECT MAX(" + nomeCulona + ") FROM " + nomeTabela;
            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public int maximo(String nomeTabela, String nomeCulona, String nomeCulonaWhere, String valorWhere) {


        int retorno = -1;

        try {

            String STRING = "SELECT MAX(" + nomeCulona + ") FROM " + nomeTabela + " WHERE " + nomeCulonaWhere + "= '" + valorWhere + "'";
            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public int maximo(String nomeTabela, String nomeCulona, String nomeCulonaWhere, String[] valorWhere) {


        int retorno = -1;

        try {

            StringBuilder in = new StringBuilder();
            in.append(" in (");
            for (int i = 0; i < valorWhere.length; i++) {
                if (i == valorWhere.length - 1) {
                    in.append("'").append(valorWhere[i]).append("'");
                } else {
                    in.append("'").append(valorWhere[i]).append("'").append(",");

                }
            }
            in.append(")");

            String STRING = "SELECT MAX(" + nomeCulona + ") FROM " + nomeTabela + " WHERE "
                    + nomeCulonaWhere + in.toString();
            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }


    public int minimo(String nomeTabela, String nomeCulona, String nomeCulonaWhere, String[] valorWhere) {


        int retorno = -1;

        try {

            StringBuilder in = new StringBuilder();
            in.append(" in (");
            for (int i = 0; i < valorWhere.length; i++) {
                if (i == valorWhere.length - 1) {
                    in.append("'").append(valorWhere[i]).append("'");
                } else {
                    in.append("'").append(valorWhere[i]).append("'").append(",");

                }
            }
            in.append(")");

            String STRING = "SELECT MIN(" + nomeCulona + ") FROM " + nomeTabela + " WHERE "
                    + nomeCulonaWhere + in.toString();
            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public int minimo(String nomeTabela, String nomeCulona, String nomeCulonaWhere, String valorWhere) {


        int retorno = -1;

        try {

            String STRING = "SELECT MIN(" + nomeCulona + ") FROM " + nomeTabela + " WHERE " + nomeCulonaWhere + "= '" + valorWhere + "'";
            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    private String maximoString(String nomeTabela, String nomeCulona, String nomeCulonaWhere, String valorWhere) {


        String retorno = "0";

        try {

            String STRING = "SELECT MAX(" + nomeCulona + ") FROM " + nomeTabela

                    + " WHERE " + nomeCulonaWhere + "= '" + valorWhere + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getString(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    private String minimoString(String nomeTabela, String nomeCulona, String nomeCulonaWhere, String valorWhere) {


        String retorno = "0";

        try {

            String STRING = "SELECT MIN(" + nomeCulona + ") FROM " + nomeTabela

                    + " WHERE " + nomeCulonaWhere + "= '" + valorWhere + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getString(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public int minimo(String nomeTabela, String nomeCulona) {


        int retorno = 0;

        try {

            String STRING = "SELECT MIN(" + nomeCulona + ") FROM " + nomeTabela;

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    /**
     * Metodo usado para contar dados de uma tabela
     *
     * @param nomeTabela nome da tabela
     * @return retorna (-1) se algo der errado ou o valor dos dados da tabela
     */
    private int minimo(String nomeTabela, String nomeCulona, String nomeCulonaWhere1, String valorWhere1, String nomeCulonaWhere2, String valorWhere2) {

        int retorno = 0;

        try {

            String STRING = "SELECT MIN(" + nomeCulona + ") FROM "
                    + nomeTabela
                    + " WHERE " + nomeCulonaWhere1 + "= '" + valorWhere1 + "' AND "
                    + nomeCulonaWhere2 + "= '" + valorWhere2 + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    private int minimoM(String nomeTabela, String nomeCulona, String nomeCulonaWhere1, String valorWhere1, String nomeCulonaWhere2, String valorWhere2) {


        int retorno = 0;

        try {

            String STRING = "SELECT MIN(" + nomeCulona + ") FROM "
                    + nomeTabela
                    + " WHERE " + nomeCulonaWhere1 + "= '" + valorWhere1 + "' AND "
                    + nomeCulonaWhere2 + ">= '" + valorWhere2 + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }


    public int maximo(String nomeTabela, String nomeCulona, String nomeCulonaWhere1, String valorWhere1, String nomeCulonaWhere2, String valorWhere2) {

        int retorno = 0;

        try {

            String STRING = "SELECT MAX(" + nomeCulona + ") FROM " + nomeTabela
                    + " WHERE " + nomeCulonaWhere1 + "= '" + valorWhere1 + "' AND "
                    + nomeCulonaWhere2 + "= '" + valorWhere2 + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public int maximoM(String nomeTabela, String nomeCulona, String nomeCulonaWhere1, String valorWhere1, String nomeCulonaWhere2, String valorWhere2) {

        int retorno = 0;

        try {

            String STRING = "SELECT MAX(" + nomeCulona + ") FROM " + nomeTabela
                    + " WHERE " + nomeCulonaWhere1 + "= '" + valorWhere1 + "' AND "
                    + nomeCulonaWhere2 + ">= '" + valorWhere2 + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    /**
     * Metodo usado para contar dados de uma tabela
     *
     * @param nomeTabela nome da tabela
     * @return retorna (-1) se algo der errado ou o valor dos dados da tabela
     */
    private int minimo(String nomeTabela, String nomeCulona, String nomeCulonaWhere1, String valorWhere1, String nomeCulonaWhere2, String valorWhere2, String nomeCulonaWhere3, String valorWhere3) {

//aqui
        int retorno = 0;

        try {

            String STRING = "SELECT MIN(" + nomeCulona + ") FROM "
                    + nomeTabela
                    + " WHERE " + nomeCulonaWhere1 + "= '" + valorWhere1 + "' AND "
                    + nomeCulonaWhere2 + "= '" + valorWhere2 + "' AND "
                    + nomeCulonaWhere3 + "= '" + valorWhere3 + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    private int minimo(String nomeTabela, String nomeCulona, String nomeCulonaWhere1, String valorWhere1, String nomeCulonaWhere2, String valorWhere2, String nomeCulonaWhere3, String valorWhere3, String nomeCulonaWhere4, String valorWhere4) {

//aqui
        int retorno = 0;

        try {

            String STRING = "SELECT MIN(" + nomeCulona + ") FROM "
                    + nomeTabela
                    + " WHERE " + nomeCulonaWhere1 + "= '" + valorWhere1 + "' AND "
                    + nomeCulonaWhere2 + "= '" + valorWhere2 + "' AND "
                    + nomeCulonaWhere4 + "= '" + valorWhere4 + "' AND "
                    + nomeCulonaWhere3 + "= '" + valorWhere3 + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }


    private int maximo(String nomeTabela, String nomeCulona, String nomeCulonaWhere1, String valorWhere1, String nomeCulonaWhere2, String valorWhere2, String nomeCulonaWhere3, String valorWhere3, String nomeCulonaWhere4, String valorWhere4) {


        int retorno = 0;

        try {

            String STRING = "SELECT MAX(" + nomeCulona + ") FROM " + nomeTabela
                    + " WHERE " + nomeCulonaWhere1 + "= '" + valorWhere1 + "' AND "
                    + nomeCulonaWhere2 + "= '" + valorWhere2 + "' AND "
                    + nomeCulonaWhere4 + "= '" + valorWhere4 + "' AND "
                    + nomeCulonaWhere3 + "= '" + valorWhere3 + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    private int maximo(String nomeTabela, String nomeCulona, String nomeCulonaWhere1, String valorWhere1, String nomeCulonaWhere2, String valorWhere2, String nomeCulonaWhere3, String valorWhere3) {


        int retorno = 0;

        try {

            String STRING = "SELECT MAX(" + nomeCulona + ") FROM " + nomeTabela
                    + " WHERE " + nomeCulonaWhere1 + "= '" + valorWhere1 + "' AND "
                    + nomeCulonaWhere2 + "= '" + valorWhere2 + "' AND "
                    + nomeCulonaWhere3 + "= '" + valorWhere3 + "'";

            Cursor cursor = lerBD().rawQuery(STRING, null);
            cursor.moveToFirst();
            retorno = cursor.getInt(0);
            cursor.close();
            fecharConecao();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public boolean getBoolean(long valor) {
        return valor > 0;
    }
}

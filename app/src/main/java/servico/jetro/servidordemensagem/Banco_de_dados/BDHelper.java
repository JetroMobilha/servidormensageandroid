package servico.jetro.servidordemensagem.Banco_de_dados;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

class BDHelper extends SQLiteOpenHelper implements BaseColumns {

    // variaveis para criasão de tabelas
    private static final String BANCO_DADOS_NOME = "servidor_mensagem";
    private static final int BANCO_DE_DAOD_VERSAO = 1;

    private String CRIATE_TABLE = "CREATE TABLE ";
    private String INTEGER = " INTEGER";
    private String UNIQUE = " UNIQUE ";
    private String VIGUL = ",";
    private String TEXT_TIPO = " TEXT";

    protected String CODIGO = "codigo";
    protected String MENSAGEM = "mensagens";
    protected String NUMERO = "numero";
    protected String TB = "tb";

    BDHelper(Context context) {
        super(context, BANCO_DADOS_NOME, null, BANCO_DE_DAOD_VERSAO);
    }

    String CRIAR_TB_MENSAGEN = CRIATE_TABLE +
            TB + "( " + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
           CODIGO + TEXT_TIPO + VIGUL +
          NUMERO + TEXT_TIPO + VIGUL +
           MENSAGEM + TEXT_TIPO + VIGUL +
            UNIQUE + "('" +  CODIGO + "')" + ")";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CRIAR_TB_MENSAGEN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TB);
        onCreate(db);
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        return super.getReadableDatabase();
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return super.getWritableDatabase();
    }
}

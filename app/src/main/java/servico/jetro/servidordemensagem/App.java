package servico.jetro.servidordemensagem;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import servico.jetro.servidordemensagem.Banco_de_dados.Crud;


public class App extends Application implements Serializable {

    private static Object mDateExpot;
    private static List<ObjetoServico> objetoServicos = new ArrayList<>();
    private static List<ObjetoServico> objetoFalhados = new ArrayList<>();
    public static int vindasDoServidor = 0;
    public static int enviadras = 0;
    private static HashMap<String,Object> hashMapDataExport;
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        Crud.init(this);
        instance = this;
    }

    public static App getInstance(){
        return instance;
    }

    public  boolean coneccao() {
        ConnectivityManager connMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void salvarImagens(List<ObjetoServico> list) {
        ArrayList<ObjetoServico> li = new ArrayList<>(list);
        Intent intent = new Intent(getApplicationContext(), SalvarDadosBD.class);
        intent.putExtra(Constantes.OBJETO,li);
        App.getInstance().getApplicationContext().startService(intent);
    }


    public static List<ObjetoServico> getObjetoServicos() {
        return objetoServicos;
    }

    public static void addObjetoServicos(ObjetoServico objetoServicos) {
        App.objetoServicos.add(objetoServicos);
    }

    public static List<ObjetoServico> getObjetoFalhados() {
        return objetoFalhados;
    }

    public static void addObjetoFalhados(ObjetoServico objetoFalhados) {
        App.objetoFalhados.add(objetoFalhados);
    }
}

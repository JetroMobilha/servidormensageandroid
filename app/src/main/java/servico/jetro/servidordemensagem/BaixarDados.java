package servico.jetro.servidordemensagem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jetro Mobilha on 26/09/2016.
 *
 * @author Jetro Mobilha
 */
public class BaixarDados extends Constantes {

    private Web web = new Web();
    private static BaixarDados instance;

    private BaixarDados() {}

    // todo : servicos
    public List<ObjetoServico> servicos() {

        List<ObjetoServico> eventosObjetoList = null;
        try {

            Map<String, Object> params = new LinkedHashMap<>();

            params.put(REQUISICAO, MENSAGEM);

            HttpURLConnection connection = web.request(Web.HOST,
                    Web.SERVIDOR, Web.POST, params, 30000);

            JSONObject object = web.resPondeJSON(connection);
            if (object.getBoolean(ESTADO))
                eventosObjetoList = criarListaObjetoServico(object.getJSONArray(OBJETO));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return eventosObjetoList;
    }

    private List<ObjetoServico> criarListaObjetoServico(JSONArray jsonArray) throws JSONException {

        List<ObjetoServico> objetoList = new ArrayList<>();
        JSONObject jsonObject1;
        for (int i = 0; i < jsonArray.length(); i++) {
            ObjetoServico objeto = new ObjetoServico();
            jsonObject1 = jsonArray.getJSONObject(i);
            if (jsonObject1.has(CODIGO)) objeto.setCodigo(jsonObject1.getString(CODIGO));
            objeto.setNumero(jsonObject1.getString(CONTACTO_1));
            objeto.setMensagem(jsonObject1.getString("corpo"));
            objetoList.add(objeto);
        }
        return objetoList;
    }

    public static BaixarDados getInstance() {
        if (instance == null) instance = new BaixarDados();
        return instance;
    }
}


package servico.jetro.servidordemensagem;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import java.util.Calendar;


/**
 * Helper class for showing and canceling notifica farras simples
 * notifications.
 * <p/>
 * This class makes heavy use of the {@link NotificationCompat.Builder} helper
 * class to create notifications in a backward-compatible way.
 */
public class Notificacao {

    private static final String NOTIFICATION_TAG = "NotificaSimples";
    private static int nomero = 3456678;


    public static void notify(final Context context) {

        String stringBuffer = " Mensagens vindas do servidor : " + App.vindasDoServidor + "\n\n" +
                " Mensagens enviadas: " + App.enviadras;

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context,context.getString(R.string.app_name))

                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentText("Funcionando -" + Calendar.getInstance().getTime())

                        // Use a default priority (recognized on devices running Android
                        // 4.1 or later)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Automatically dismiss the notification when it is touched.
                .setAutoCancel(false);
        String[] events = new String[6];
        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle("Informação")
        .setSummaryText("Estado do funcionamento do servidor")
                .addLine("" + Calendar.getInstance().getTime())
                .addLine("Mensagens vindas do servidor : " + App.vindasDoServidor)
                .addLine( "Mensagens enviadas: " + App.enviadras) ;

        for (int i=0; i < events.length; i++) {
          //  inboxStyle.addLine(events[i]);
        }

        builder.setStyle(inboxStyle);
        notify(context, builder.build());

    }


    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        assert nm != null;
        nm.notify(NOTIFICATION_TAG, nomero, notification);
    }

    /**
     * Cancels any notifications of this type previously shown using
     */
    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        assert nm != null;
        nm.cancel(NOTIFICATION_TAG, nomero);
    }
}

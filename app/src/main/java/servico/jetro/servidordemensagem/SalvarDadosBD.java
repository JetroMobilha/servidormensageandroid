package servico.jetro.servidordemensagem;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.LinkedList;
import java.util.List;

import servico.jetro.servidordemensagem.Banco_de_dados.Crud;

public class SalvarDadosBD extends Service {

    public SalvarDadosBD(){}

    private LinkedList<List<Object>> objectList;
    private boolean salvando = false;
    private static int control = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        objectList = new LinkedList<>();
    }

    public void setObjectList(List<Object> object) {
        objectList.offer(object);
    }

    private void salvar(List<Object> o) {
        //noinspection unchecked
        new Salvar().execute(o);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getExtras() != null && intent.getExtras().get(Constantes.OBJETO) != null) {

            @SuppressWarnings("unchecked")
            List<Object> object = (List<Object>) intent.getExtras().get(Constantes.OBJETO);
            if (object != null && object.size() > 0) {
                setObjectList(object);
                emServico();
            }
        }


        if (objectList.peek() != null && !salvando) {
            salvando = true;
            salvar(objectList.poll());
        }


        if (control == 0) stopSelf();

        return START_STICKY;
    }

    @SuppressLint("StaticFieldLeak")
    private class Salvar extends AsyncTask<List<Object>, Void, Void> {

        @Override
        protected void onPreExecute() {
            salvando = true;
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(List<Object>... params) {

            for (Object object : params[0]) {
                if (object instanceof ObjetoServico) {
                    salvarEntidade((ObjetoServico) object);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            salvando = false;

            if (objectList.peek() != null) {

                onStartCommand(null, 0, 0);
            } else {
                foraSerco();
            }
        }

        private void salvarEntidade(ObjetoServico entidades) {
             Crud.getInstance().criaMensagens(entidades);
        }
    }

    private void emServico() {
        control++;
    }

    private void foraSerco() {
        control--;
        if (control <= 0)
            stopSelf();
    }
}

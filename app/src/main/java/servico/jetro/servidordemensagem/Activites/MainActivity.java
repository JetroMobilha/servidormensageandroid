package servico.jetro.servidordemensagem.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import servico.jetro.servidordemensagem.R;
import servico.jetro.servidordemensagem.Servico.ServicoMensagen;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView button = new TextView(this);
        button.setText(R.string.app_name);
        setContentView(button);

        sendBroadcast(new Intent("servico.jetro.servidor.mensagem.Receiver"));
      //  startService(new Intent(getApplicationContext(), ServicoMensagen.class));
        Toast.makeText(getApplicationContext(),"Servico Iniciado veras os primeros resultado dentro de 5 minutos",Toast.LENGTH_LONG).show();
      finish();
    }
}

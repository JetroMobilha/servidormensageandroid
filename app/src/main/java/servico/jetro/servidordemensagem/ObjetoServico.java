package servico.jetro.servidordemensagem;

import java.io.Serializable;

/**
 * Created by Jetro Domigos on 13/05/2018.
 */

public class ObjetoServico implements Serializable {

    private int id;
    private String codigo;
    private String mensagem;
    private String numero;

    public ObjetoServico(){
    }

    public ObjetoServico(String codigo){
        setCodigo(codigo);
    }


    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        return  obj!= null && this.codigo.equals(((ObjetoServico)obj).codigo);
    }
}
